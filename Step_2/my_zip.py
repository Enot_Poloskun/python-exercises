import sys


def zip_func(*lists):

    sentinel = object()
    parts = [iter(it) for it in lists]
    while parts:
        result = []
        for it in parts:
            elem = next(it, sentinel)
            if elem is sentinel:
                return
            result.append(elem)
        print result

def main():

    a = [1, 2, 3]
    b = [4, 5, 6]
    c = [7, 8, 9]
    zip_func(a, b,c)


if __name__ == '__main__':
    main()
