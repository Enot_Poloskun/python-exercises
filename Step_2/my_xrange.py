import itertools

def xrange_func(start, stop, step):

       for i in itertools.count(start, step):
           yield i
           if i>stop-2*step:
            break


def main():
    start = 10
    stop =40
    step = 5
    mygen = xrange_func(start, stop, step)
    test_list = list(xrange(start, stop, step))
    print 'real xrange : ', test_list
    # for i in mygen:
    #     print (i)
    my_list = [i for i in mygen]
    print 'my xrange : ', my_list



if __name__=='__main__':
        main()