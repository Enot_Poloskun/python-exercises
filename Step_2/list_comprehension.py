import sys


def squared(list_of_numbers):
    return [x*x for x in list_of_numbers]


def odd_elements(list_of_numbers):
    return [x for i, x in enumerate(list_of_numbers) if i % 2 != 0]


def squared_even_odds(list_of_numbers):
    return squared([x for x in odd_elements(list_of_numbers) if x % 2 == 0])


def print_usage():
    print('use \'squared \' option for getting list of squared input elements')
    print('use \'odd\' option for getting list of each second element from input list')
    print('use \'even_odd \' option for getting quadration of all even elements on the odd positions')
    print('use \'quit\' to quit')


def main():
    if len(sys.argv) > 1:
        print('run script without arguments please')
        sys.exit(1)

    while True:
        print('enter your space separated elements of list please')
        try:
            list_of_numbers = map(int, raw_input().split())
        except ValueError:
            print('Invalid numbers!')
            continue
        break
    print(list_of_numbers)
    print_usage()
    while True:
        print('enter option please')
        option = raw_input()
        if option == 'squared':
            print(squared(list_of_numbers))
        elif option == 'odd':
            print(odd_elements(list_of_numbers))
        elif option == 'even_odd':
            print(squared_even_odds(list_of_numbers))
        elif option == 'quit':
            sys.exit(0)
        else:
            print('unknown option \'{}\''.format(option))
            print_usage()

if __name__ == '__main__':
    main()
