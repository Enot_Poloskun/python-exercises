import os
import re
import sys
import urllib
from pprint import pprint

def main():
    filename = sys.argv[1]
    file = open(filename)
    server_name = filename[filename.index('_')+1:]
    url_list = {}
    count=0
    for line in file:
        url_match = re.search(r'"GET (\S+)', line)
        if url_match:
            path = url_match.group(1)
            url_list['http:/'+ path] = 1
            count = count+1
    file.close()
    pprint(url_list)
    print count
if __name__ =='__main__':
    main()