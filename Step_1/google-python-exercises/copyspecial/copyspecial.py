#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys
import re
import os
import shutil
import commands

"""Copy Special exercise
"""

# +++your code here+++
# Write functions and modify main() to call them
def get_special_paths(directory):
    paths = []
    input_files = os.listdir(directory)
    for files in input_files:
        path = re.match(r'___(\w+)__', files)
        if path:
            paths.append(os.path.abspath(directory, files))
    return paths


def copy_to(paths, direct):
#copy given paths to the given directory
#if destination dir doesnt exist---> create given dir
      if not os.path.exists(direct):
          os.mkdir(direct)
      for path in paths:
          file = os.path.basename(path)
          shutil.copy(path, os.path.join(direct, file) )


def zip_to(paths, zippath):
    (status, output) = commands.getstatusoutput()
#given a list of paths. zip those files up into the given zipfile
    message = "Command I'm going to do: zip -j" + zippath + ' ' + ' '.join(paths)
    print message
    if status:
        sys.stderr.write(output)
        sys.exit(1)




def main():
  # This basic command line argument parsing code is provided.
  # Add code to call your functions below.

  # Make a list of command line arguments, omitting the [0] element
  # which is the script itself.
  args = sys.argv[1:]
  if not args:
    print "usage: [--todir dir][--tozip zipfile] dir [dir ...]";
    sys.exit(1)

  # todir and tozip are either set from command line
  # or left as the empty string.
  # The args array is left just containing the dirs.
  todir = ''
  if args[0] == '--todir':
    todir = args[1]
    del args[0:2]

  tozip = ''
  if args[0] == '--tozip':
    tozip = args[1]
    del args[0:2]

  if len(args) == 0:
    print "error: must specify one or more dirs"
    sys.exit(1)

  # +++your code here+++
  # Call your functions
  path = []
  for directory in args:
      path.extend(get_special_paths(directory))
      if todir:
          copy_to(path, direct)
      elif tozip:
           zip_to(paths, zippath)
      else:
        print '\n'.join(path)

if __name__ == "__main__":
  main()
