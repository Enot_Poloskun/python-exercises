import sys
import requests
import json
import re
import itertools

from lxml import etree
from datetime import datetime, date, timedelta

import constants


def fill_ajax_params(flight_params):
    ajax_params = {'_ajax[templates][]': 'main',  # ['main', 'priceoverview', 'flightinfo'],
                   '_ajax[requestParams][departure]': flight_params['departure'],
                   '_ajax[requestParams][destination]': flight_params['destination'],
                   '_ajax[requestParams][returnDeparture]': '',
                   '_ajax[requestParams][returnDestination]': '',
                   '_ajax[requestParams][outboundDate]': flight_params['outbounddate'],
                   '_ajax[requestParams][returnDate]': flight_params['returndate'],
                   '_ajax[requestParams][adultCount]': '1',
                   '_ajax[requestParams][childCount]': '0',
                   '_ajax[requestParams][infantCount]': '0',
                   '_ajax[requestParams][openDateOverview]': '',
                   '_ajax[requestParams][oneway]': '1' if flight_params['oneway'] else '0'}
    return ajax_params


def fill_request_params(flight_params):
    request_params = {'departure': flight_params['departure'],
                      'destination': flight_params['destination'],
                      'outboundDate': flight_params['outbounddate'],
                      'returnDate': flight_params['returndate'],
                      'oneway': '1' if flight_params['oneway'] else '0',
                      'openDateOverview': '0',
                      'adultCount': '1',
                      'childCount': '0',
                      'infantCount': '0'}
    return request_params


def get_session_url(session, flight_params):
    session.get(constants.URL_START)
    response = session.get(constants.URL_VACANCY, params=fill_request_params(flight_params))
    return response.url


def extract_main_section_etree(session, session_url, flight_params):
    response = session.post(session_url, data=fill_ajax_params(flight_params), headers=constants.HEADER_DICT)
    data = json.loads(response.text)
    error = None
    html = None
    if 'templates' in data and 'main' in data['templates'] and len(data['templates']['main']) > 0:
        html = etree.HTML(data['templates']['main'])
    if 'errorRAW' in data:
        error = 'unknown'
        if len(data['errorRAW']) > 0:
            error = data['errorRAW'][0].get('code', 'unknown')
            if len(error) == 0:
                error = 'no_flights'
    return html, error


class FlightInfo:
    start_time = None
    end_time = None
    duration = None
    ticket_class = ''
    price = 0

    # Example string: 'VIE-DUS, 17:05-18:35, 01 h 30 min , Economy Classic: 6.407,00'
    def __init__(self, description_str):
        tokens = [token.strip() for token in description_str.split(',', 3)]
        time_str = tokens[1]
        duration_str = tokens[2]
        ticket_class_str, ticket_price_str = tokens[3].split(':')

        start_time_str, end_time_str = time_str.split('-')
        self.start_time = datetime.strptime(start_time_str.strip(), '%H:%M').time()
        self.end_time = datetime.strptime(end_time_str.strip(), '%H:%M').time()

        match = re.match(r'(\d+)\s*h\s*(\d+)\s*min', duration_str)
        if match is not None:
            try:
                hours = int(match.group(1))
                minutes = int(match.group(2))
                self.duration = timedelta(hours=hours, minutes=minutes)
            except ValueError:
                pass

        self.ticket_class = ticket_class_str.strip()

        ticket_price_str = ticket_price_str.strip()
        try:
            integer_part = int(filter(lambda x: x.isdigit(), ticket_price_str[0:-3]))
            fractional_part = float(ticket_price_str[-2:])
            self.price = integer_part + fractional_part/100
        except ValueError:
            pass

    def __repr__(self):
        start_time_str = self.start_time.strftime('%H:%M')
        end_time_str = self.end_time.strftime('%H:%M')
        duration_str = '{:02d} h {:02d} min'.format(*divmod(self.duration.seconds/60, 60))
        ticket_class_str = self.ticket_class
        ticket_price_str = '{:.2f}'.format(self.price)
        return '{} - {}, {}, {}: {}'.format(
            start_time_str, end_time_str, duration_str, ticket_class_str, ticket_price_str)


class TwoWayFlightInfo:
    outbound_flight = None
    return_flight = None

    def __init__(self, outbound_flight, return_flight):
        self.outbound_flight = outbound_flight
        self.return_flight = return_flight

    @property
    def total_price(self):
        if self.outbound_flight is None or self.return_flight is None:
            return -1
        return self.outbound_flight.price + self.return_flight.price

    def __repr__(self):
        result = 'Outbound: {}\tReturn:   {}\tTotal price: {:.2f}'.format(
            self.outbound_flight, self.return_flight, self.total_price)
        return result


def extract_flights(html):
    blocks = {'outbound block': [],
              'return block': []}
    for block, flights in blocks.items():
        block_elem = html.xpath(r'//*[@class="{}"]'.format(block))[0]
        flightrows = block_elem.xpath(r'.//*[contains(@class, "flightrow")]')
        titles_met = set()
        for flightrow in flightrows:
            title_elems = flightrow.xpath(r'.//label[contains(@id, "priceLabel")]//span[@title]')
            for title_elem in title_elems:
                title_str = title_elem.get('title')
                if title_str in titles_met:
                    continue
                titles_met.add(title_str)
                flight_info = FlightInfo(title_str)
                if flight_info.price != 0:
                    flights.append(flight_info)
    return blocks['outbound block'], blocks['return block']


def extract_currency(html):
    currency = html.xpath(r'//*[contains(@id, "flight-table-header-price")]')
    return currency[0].text.strip()


def print_usage():
    print 'enter your data in the following format:'
    print 'departure{IATA code} destination{IATA code} outbound_date{yyyy-mm-dd} [returndate] is optional'


def dates_valid(outbound_date_str, return_date_str):
    today = date.today()
    try:
        outbound_date = datetime.strptime(outbound_date_str, '%Y-%m-%d').date()
    except ValueError:
        print 'Outbound date is incorrect'
        return False
    if outbound_date < today:
        print 'Outbound date should be after today'
        return False
    if return_date_str is not None and len(return_date_str) > 0:
        try:
            return_date = datetime.strptime(return_date_str, '%Y-%m-%d').date()
        except ValueError:
            print 'Return date is incorrect'
            return False
        if return_date < today:
            print 'Return date should be after today'
            return False
        if return_date < outbound_date:
            print 'Return date should be after Outbound date'
            return False
    return True


def iata_valid(iata_str):
    match = re.match(r'[a-zA-Z]{3}', iata_str)
    return match is not None


def main():
    returndate = ''
    if len(sys.argv) == 4:
        oneway = True
    elif len(sys.argv) == 5:
        oneway = False
        returndate = sys.argv[4]
    else:
        print_usage()
        sys.exit(1)
    departure = sys.argv[1]
    destination = sys.argv[2]
    outbounddate = sys.argv[3]

    if not iata_valid(departure):
        print 'Invalid departure code'
        sys.exit(1)
    if not iata_valid(destination):
        print 'Invalid destination code'
        sys.exit(1)

    if not dates_valid(outbounddate, returndate):
        sys.exit(1)

    with requests.session() as session:
        flight_params = {'departure': departure,
                         'destination': destination,
                         'outbounddate': outbounddate,
                         'returndate': returndate,
                         'oneway': oneway}

        session_url = get_session_url(session, flight_params)
        html, error = extract_main_section_etree(session, session_url, flight_params)
        if error is not None:
            if error != 'unknown' and error in constants.ERROR_MESSAGES:
                print 'Error: {}'.format(constants.ERROR_MESSAGES[error])
            else:
                print 'Unknown error: {}'.format(error)
            sys.exit(1)
        if html is None:
            print 'Error: Could not read response from server'
            sys.exit(1)
        print 'Currency:', extract_currency(html)
        outbound_flights, return_flights = extract_flights(html)
        if not oneway:
            flight_combinations = [
                TwoWayFlightInfo(outbound_flight, return_flight)
                for outbound_flight, return_flight
                in itertools.product(outbound_flights, return_flights)
            ]
            for flight_info in sorted(flight_combinations, key=lambda x: x.total_price):
                print flight_info
        else:
            for flight_info in sorted(outbound_flights, key=lambda x: x.price):
                print flight_info
        sys.exit(1)


if __name__ == '__main__':
    main()
